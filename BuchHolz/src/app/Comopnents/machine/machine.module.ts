import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MachineComponent } from './machine.component';
import { userLayoutModule } from '../layouts/user-layout/user-layout.module';
import { PanelModule } from 'primeng/panel';
import { Routes, RouterModule } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ListboxModule } from 'primeng/listbox';
import { global } from 'src/app/global';
import { PanelMenuModule } from 'primeng/panelmenu';
import { AddMachineModule } from '../machine/add-machine/add-machine.module';
import { ViewMachineModule } from './view-machine/view-machine.module';
import { AttachCustomersModule } from './attach-customers/attach-customers.module';


@Injectable({
  providedIn: 'root',
})

@NgModule({
  declarations: [MachineComponent],
  imports: [
    CommonModule,
    userLayoutModule,
    PanelModule,
    RouterModule,
    ListboxModule,
    PanelMenuModule,
    AddMachineModule,
    ViewMachineModule,
    AttachCustomersModule
  ]
})
export class MachineModule { }
