import { Component, OnInit } from '@angular/core';
import { MachineService } from 'src/app/Services/machine.service';
import { Machine } from 'src/app/Models/Machine';

@Component({
  selector: 'app-view-machine',
  templateUrl: './view-machine.component.html',
  styleUrls: ['./view-machine.component.css']
})
export class ViewMachineComponent implements OnInit {

  AllMachines: Machine[] = [];

  constructor(private machineServ: MachineService) {

   }

  ngOnInit(): void {
    
    this.machineServ.getAllMachines().then(machines => {
      machines._Data.map(machine => {
        debugger
        this.AllMachines.push(machine)
      })
    })
   }

}
