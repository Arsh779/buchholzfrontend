import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewMachineComponent } from './view-machine.component';
import { TableModule } from 'primeng/table';
import { MachineService } from 'src/app/Services/machine.service';


@NgModule({
  declarations: [ViewMachineComponent],
  imports: [
    CommonModule,
    TableModule
  ],
  providers: [
    MachineService
  ]
})
export class ViewMachineModule { }
