import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.css']
})
export class MachineComponent implements OnInit {
  items: any[] = []
  constructor() { }

  ngOnInit(): void {
    this.items = [
      {
          label: 'Add Machine',
          icon: 'pi pi-pw pi-file',
          items: [{
              label: 'Add Machine',
              routerLink: "./addMachine"
          },
          { 
              label: 'View Machines',
              routerLink: "./viewMachines"
          },
          { 
              label: 'Attach Customers',
              routerLink: "./attachMachine"
          },
        ]
      },
      {
          label: 'Edit',
          icon: 'pi pi-fw pi-pencil',
          items: [
              { label: 'Delete', icon: 'pi pi-fw pi-trash' },
              { label: 'Refresh', icon: 'pi pi-fw pi-refresh' }
          ]
      }
  ];
  }

}
