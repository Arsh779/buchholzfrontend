import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachCustomerComponent } from './attach-customer.component';
import { MachineService } from 'src/app/Services/machine.service';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { TabViewModule } from 'primeng/tabview';
import { CustomerService } from 'src/app/Services/customer.service';

@NgModule({
  declarations: [AttachCustomerComponent],
  imports: [
    CommonModule,
    TableModule,
    InputTextModule,
    FormsModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    MessagesModule,
    MessageModule,
    TabViewModule,
    ReactiveFormsModule
  ],
  providers: [
    MachineService,
    CustomerService,
    TableModule
  ]
})
export class AttachCustomersModule { }
