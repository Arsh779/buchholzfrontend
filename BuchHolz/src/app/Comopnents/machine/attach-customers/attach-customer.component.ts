import { Component, OnInit, ViewChild } from '@angular/core';
import { Machine } from 'src/app/Models/Machine';
import { MachineService } from 'src/app/Services/machine.service';
import { Table } from 'primeng/table';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/Services/customer.service';
import { dropDown } from 'src/app/Models/dropDown';
import { customerMachine } from 'src/app/Models/customer';

@Component({
  selector: 'app-attach-customer',
  templateUrl: './attach-customer.component.html',
  styleUrls: ['./attach-customer.component.css']
})
export class AttachCustomerComponent implements OnInit {
  @ViewChild('dt', {static: true}) table: Table;
  AllMachines: Machine[] = [];
  Customers: dropDown[] = [];
  loading: boolean = true;
  Machine: Machine;
  machineModal: boolean = false
  userform: FormGroup;
  selectedMachine: Machine;
  message: string

  constructor(private machineServ: MachineService, private custServ: CustomerService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.machineServ.getAllMachines().then(machines => {
      machines._Data.map(machine => {
        this.AllMachines.push(machine)
      })
      this.loading = false;
    });

    this.custServ.getAllCustomer().then(customers => {      
      customers._Data.map(customer => {
        
        let custs = new dropDown();
        custs.label = customer.customerName,
        custs.value = customer.customerId.toString()

        this.Customers.push(custs);
      })
    });

    this.userform = this.fb.group({
      'CustomerId': new FormControl('', Validators.required),
    });

    this.reset();
  }

  reset(){
    this.table.reset();
  }

  onSubmit(userForm){
    let cust_Ma = new customerMachine()
    cust_Ma.customerId = userForm.CustomerId
    cust_Ma.machineId = parseInt(this.Machine.machineId);

    this.saveMachineForCustomer(cust_Ma);
  }

  onRowSelect(event)
  {
    this.Machine.machineName = event.data.machineName
    this.Machine.machineNumber = event.data.machineNumber

    let mcId = event.data.machineId ?? 0;
    
    this.machineModal = true
  }

  saveMachineForCustomer(data: customerMachine){
    debugger;
    let custMc = new customerMachine

    custMc.customerId = parseInt(data.customerId.toString()) 
    custMc.machineId = parseInt(data.machineId.toString()) 

    this.custServ.attachCustomer(custMc).then(response => {
      this.message = response.message;      
    })
    debugger;
    this.machineModal = false
  }

  
}



