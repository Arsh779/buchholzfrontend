import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MachineService } from 'src/app/Services/machine.service';
import { dropDown } from 'src/app/Models/dropDown';
import { Machine } from 'src/app/Models/Machine';
import { CustomerService } from 'src/app/Services/customer.service';

@Component({
  selector: 'app-add-machine',
  templateUrl: './add-machine.component.html',
  styleUrls: ['./add-machine.component.css']
})
export class AddMachineComponent implements OnInit {

  userform: FormGroup;
  index: number = 0;
  MachineSpeciality: dropDown[] = [];
  MachineStatus: dropDown[] = [];
  MachineType: dropDown[] = [];
  MachineAttributeType: dropDown[] = [];
  MachineManufacturer: dropDown[] = [];
  PreviousCustomers: dropDown[] = [];

  constructor(private fb: FormBuilder, private machineServ: MachineService, private custServ: CustomerService) { }

  ngOnInit(): void {
    this.userform = this.fb.group({
      'MachineNumber': new FormControl('', Validators.required),
      'MachineName': new FormControl('', Validators.required),
      'Address': new FormControl('', Validators.required),
      'LocationOnMap': new FormControl(false),
      'Lattitude': new FormControl('', [
        this.requiredIfValidator(() => this.userform.get('LocationOnMap').value)
      ]),
      'Longitude': new FormControl('', [
        this.requiredIfValidator(() => this.userform.get('LocationOnMap').value)
      ]),
      'TestDate': new FormControl('', Validators.required),
      'DateOfManufacture': new FormControl('', Validators.required),
      'DateOfSelling': new FormControl('', Validators.required),
      'Barcode': new FormControl('', Validators.required),
      'UvvValidDate': new FormControl('', Validators.required),
      'MachineSpecialtyId': new FormControl('', Validators.required),
      'ManufacturerId': new FormControl('', Validators.required),
      'MachineStatusId': new FormControl('', Validators.required),
      'MachineTypeId': new FormControl('', Validators.required),
      'MachineAttributes': this.machineAttributeForm()
    });
    debugger;
    this.Initialize()
  }

  requiredIfValidator(predicate) {
    return (formControl => {
      if (!formControl.parent) {
        return null;
      }
      if (predicate()) {
        return Validators.required(formControl);
      }
      return null;
    })
  }

  machineAttributeForm()
  {
    let AttributeForm =  this.fb.group({
      machineryType: new FormControl('', Validators.required),
      customerDeviceDelivered: new FormControl(false),
      deliveryDate: new FormControl('', [
        this.requiredIfValidator(() => AttributeForm.get('customerDeviceDelivered').value)
      ]),
      garunteeEndDate: new FormControl('', Validators.required),
      usedMachine: new FormControl(false),
      previousOwner: new FormControl('', [
        this.requiredIfValidator(() => AttributeForm.get('usedMachine').value)
      ]),
      isScrapped: new FormControl(false),
      scrappedDate: new FormControl('', [
        this.requiredIfValidator(() => AttributeForm.get('isScrapped').value)
      ]),
      
    });

    return AttributeForm;    
  }

  onSubmit(userform) {
    debugger;

    let machine = new Machine();

    machine.barcode = userform.Barcode
    machine.address = userform.Address
    machine.dateOfManufacture = userform.DateOfManufacture
    machine.dateOfSelling = userform.DateOfSelling
    machine.lattitude = userform.Lattitude
    machine.longitude = userform.Longitude
    machine.machineNumber = userform.MachineNumber
    machine.machineSpecialtyId = userform.MachineSpecialtyId
    machine.manufacturerId = userform.ManufacturerId
    machine.uvvValidDate = userform.UvvValidDate
    machine.machineStatusId = userform.MachineStatusId
    machine.machineTypeId = userform.MachineTypeId
    machine.testDate = userform.TestDate
    machine.machineName = userform.MachineName

    this.machineServ.saveMachine(machine);
  }

  openNext() {
    this.index = (this.index === 2) ? 0 : this.index + 1;
  }

  openPrev() {
    this.index = (this.index === 0) ? 2 : this.index - 1;
  }

  /**
   * This function is responsible for initalizing thing like 
   * drops-downs, etc
   */
  Initialize(): void {

    this.machineServ.getMachineSpeciality().then(response => {
      response._Data.map(value => {
        let ddl = new dropDown();
        ddl.label = value.specialty
        ddl.value = value.machineSpecialtyId
        this.MachineSpeciality.push(ddl);
      });
    });

    this.machineServ.getMachineManufacturer().then(response => {
      response._Data.map(value => {
        let ddl = new dropDown();
        ddl.label = value.manufacturer
        ddl.value = value.manufacturerId
        this.MachineManufacturer.push(ddl);
      });
    });

    this.machineServ.getMachineStatus().then(response => {
      response._Data.map(value => {
        let ddl = new dropDown();
        ddl.label = value.machineStatus
        ddl.value = value.machineStatusId
        this.MachineStatus.push(ddl);
      });
    });

    this.machineServ.getMachineType().then(response => {
      response._Data.map(value => {
        let ddl = new dropDown();
        ddl.label = value.machineType
        ddl.value = value.machineTypeId
        this.MachineType.push(ddl);
      });
    });

    this.machineServ.getMachineTypeAttribute().then(response => {
      response._Data.map(value => {
        let ddl = new dropDown()
        ddl.label = value.machineType
        ddl.value = value.machineTypeId.toString()
        this.MachineAttributeType.push(ddl)
      })
    })

    this.custServ.getAllCustomer().then(response => {
      response._Data.map(value => {
        let ddl = new dropDown()
        ddl.label = value.customerName
        ddl.value = value.customerId.toString()
        this.PreviousCustomers.push(ddl);
      })
    })
  }

  onBasicUpload(event)
  {
    
  }

}
