import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddMachineComponent } from './add-machine.component';
import { CalendarModule } from 'primeng/calendar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { TabViewModule } from 'primeng/tabview';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { MachineService } from 'src/app/Services/machine.service';
import { CustomerService } from 'src/app/Services/customer.service';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';

@NgModule({
  declarations: [AddMachineComponent],
  imports: [
    CommonModule,
    InputTextModule,
    CalendarModule,
    MessagesModule,
    MessageModule,
    TabViewModule,
    CheckboxModule,
    InputTextareaModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    FieldsetModule,
    FileUploadModule
  ],
  providers: [
    MachineService,
    CustomerService
  ]
})
export class AddMachineModule { }
