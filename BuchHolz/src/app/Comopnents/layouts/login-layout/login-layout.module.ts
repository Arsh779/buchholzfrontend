import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginLayoutComponent } from './login-layout.component';



@NgModule({
  declarations: [LoginLayoutComponent],
  imports: [
    CommonModule
  ],
  exports: [
    LoginLayoutComponent
  ]

})
export class LoginLayoutModule { }
