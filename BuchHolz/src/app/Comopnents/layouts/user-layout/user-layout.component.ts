import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
// import { globalValues } from 'src/app/app.global';

@Component({
  selector: 'app-user-layout',
  templateUrl: './user-layout.component.html',
  styleUrls: ['./user-layout.component.css']
})
export class UserLayoutComponent implements OnInit {
  items: MenuItem[];
  userName: string;
  constructor() { }

  ngOnInit(): void {
    this.userName = sessionStorage.getItem("username");

    this.items = [
      {
        label: 'Jobs',
        items: [{
          label: 'Add Jobs',
          icon: 'pi pi-fw pi-plus',
          routerLink: ['/editPassword']
        },
        {
          label: 'Assign Jobs',
          icon: 'pi pi-fw pi-plus',
          routerLink: ['/addUser'],
          items: [
            { label: 'Project' },
            { label: 'Other' },
          ]
        }]
      },
      {
        label: 'Stocks',
        items: [{
          label: 'New',
          icon: 'pi pi-fw pi-plus',
          routerLink: ['/stocks']
        },
        {
          label: 'Add New',
          icon: 'pi pi-fw pi-plus',
          routerLink: ['/stocks/AddStocks'],
          items: [
            { label: 'Project' },
            { label: 'Other' },
          ]
        }]
      },
      {
        label: 'Article',
        items: [
          { 
            label: 'Add Article', 
            routerLink: ['/article'], 
            icon: 'pi pi-fw pi-plus' 
          },
          { 
            label: 'Refresh', 
            // routerLink: ['/viewArticle'], 
            icon: 'pi pi-fw pi-refresh' 
          }
        ]
      },
      {
        label: 'Kunden',
        items: [
          { 
            label: 'Kundenbetrieb', 
            routerLink: ['/customer'], 
            icon: 'pi pi-fw pi-plus',
            items: [
              { 
                label: 'Kunden hinzufügen', 
                routerLink: ['/customer/addCustomer'], 
                icon: 'pi pi-fw pi-plus',
              }
            ]
          },
          { 
            label: 'Refresh', 
            // routerLink: ['/viewArticle'], 
            icon: 'pi pi-fw pi-refresh' 
          }
        ]
      },
      {
        label: 'Maschine',
        items: [
          { 
            label: 'Maschine', 
            routerLink: ['/machine'], 
            icon: 'pi pi-fw pi-plus',
            items: [
              { 
                label: 'Kunden hinzufügen', 
                routerLink: ['/machine/addMachine'], 
                icon: 'pi pi-fw pi-plus',
              }
            ]
          },
          { 
            label: 'Refresh', 
            // routerLink: ['/viewArticle'], 
            icon: 'pi pi-fw pi-refresh' 
          }
        ]
      }
    ];
  }

  addNewUser(event) {
    debugger;
  }

}
