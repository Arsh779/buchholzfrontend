import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLayoutComponent } from './user-layout.component'
import { ButtonModule } from 'primeng/button';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';

@NgModule({
  declarations: [UserLayoutComponent],
  imports: [
    CommonModule,
    TieredMenuModule,
    ButtonModule,
    MenubarModule
  ],
  exports: [
    ButtonModule,
    UserLayoutComponent
  ]
})
export class userLayoutModule { }
