import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { PanelModule } from 'primeng/panel';


@NgModule({
  declarations: [DefaultComponent],
  imports: [
    CommonModule,
    PanelModule
  ]
})
export class DefaultModule { }
