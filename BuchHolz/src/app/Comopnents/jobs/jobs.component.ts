import { Component, OnInit, ViewChild } from '@angular/core';
import { Materialverbrauch } from 'src/app/Models/Auftrag';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Table } from 'primeng/table/table';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  @ViewChild('table', { static: true }) ArticleTable: Table;
  @ViewChild('EmployeeSignature', { static: true }) EmpSignature: any;
  @ViewChild('KundenSignature', { static: true }) KundSignature: any;

  
  today: Date = (new Date());

  userform: FormGroup;
  auftragForm: FormGroup;
  selectedArticle: string;

  clearClass: string = 'clear-signature';
  cleatText: string = 'löschen';

  materialList: Materialverbrauch[] = [];

  articles: any[] = [
    { label: 'Material auswählen', value: null },
    { label: "item one", value: 'RM-110' },
    { label: "item two", value: 'RM-111' },
    { label: "item three", value: 'RM-112' },
    { label: "item four", value: 'RM-113' },
    { label: "item five", value: 'RM-114' }
  ]

  columns: number[];

  selectedState: any = null;

  states: any[] = [
    { name: 'Arizona', code: 'Arizona' },
    { name: 'California', value: 'California' },
    { name: 'Florida', code: 'Florida' },
    { name: 'Ohio', code: 'Ohio' },
    { name: 'Washington', code: 'Washington' }
  ];

  cities1: any[] = [];

  cities2: any[] = [];

  city1: any = null;

  city2: any = null;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    
    this.today.setDate(new Date().getDate())
    this.columns = [0, 1, 2, 3, 4, 5];
    this.userform = this.fb.group({
      'stock': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required),
      'price': new FormControl('', Validators.required),
      'articleNumber': new FormControl('', Validators.required)
    });

    this.auftragForm = this.fb.group({
      "customerNumber": new FormControl('', Validators.required),
      "strasse": new FormControl('', Validators.required),
      "postal": new FormControl('', Validators.required),
      "telephone" : new FormControl('', Validators.required),
      "mobile": new FormControl('', Validators.required),
      "deviceType": new FormControl('', Validators.required),
      "serialNumber": new FormControl('', Validators.required),
      "requestDate" : new FormControl('', Validators.required),
      "employeeName" : new FormControl('', Validators.required),
      "Auslieferung" : new FormControl('', Validators.required),
      "Reparatur" : new FormControl('', Validators.required),
      "Wartung" : new FormControl('', Validators.required),
      "Sonstiges" : new FormControl('', Validators.required),
      "besteilNr" : new FormControl('', Validators.required),
      "mail" : new FormControl('', Validators.required),
      "description" : new FormControl('', Validators.required),
      "date" : new FormControl('', Validators.required),
      "hours" : new FormControl('', Validators.required),
      "drivingZone" : new FormControl('', Validators.required),
      "empSignature" : new FormControl('', Validators.required),
      "custSignature" : new FormControl('', Validators.required),
    })
  }
  private reset() {

    this.ArticleTable.reset();
  }

  onSubmit(userform: any) {
    debugger;
    let Matbrauch = new Materialverbrauch()
    Matbrauch.stock = userform.stock
    Matbrauch.description = userform.description
    Matbrauch.price = userform.price
    Matbrauch.articleNumber = userform.articleNumber

    this.materialList.push(Matbrauch);
    this.reset();

  }

  generateAuftrag(value)
  {
    debugger;
    let t1= this.EmpSignature.signaturePad.toDataURL();
    let t2 = this.KundSignature.signaturePad.toDataURL();
    debugger;

  }

  showImage(event) {
  }

  addColumn() {
    
    this.columns.push(this.columns.length);  
  }

  removeColumn() {
    this.materialList.splice(-1, 1);
  }

}
