import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StocksComponent } from './stocks.component';
import { userLayoutModule } from '../layouts/user-layout/user-layout.module';
import { PanelModule } from 'primeng/panel';
import { Routes, RouterModule } from '@angular/router';
import { AddStockModule } from './add-stock/add-stock.module';
import { FormBuilder } from '@angular/forms';
import { ListboxModule } from 'primeng/listbox';
import { StockService } from 'src/app/Services/stock.service';
import { global } from 'src/app/global';
import { PanelMenuModule } from 'primeng/panelmenu';

@Injectable({
  providedIn: 'root',
})

@NgModule({
  declarations: [StocksComponent],
  imports: [
    CommonModule,
    userLayoutModule,
    PanelModule,
    RouterModule,
    AddStockModule,
    ListboxModule, 
    PanelMenuModule
  ],
  providers: [
    StockService,
    global
  ]
})
export class StocksModule {

}
