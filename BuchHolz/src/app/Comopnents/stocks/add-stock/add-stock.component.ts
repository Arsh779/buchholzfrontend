import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { StockService } from 'src/app/Services/stock.service';
import { dropDown } from 'src/app/Models/dropDown';
import { stock } from 'src/app/Models/stock';
import { ConnectionService } from 'ng-connection-service'
import { TabView } from 'primeng/tabview';


@Component({
  selector: 'app-add-stock',
  templateUrl: './add-stock.component.html',
  styleUrls: ['./add-stock.component.css']
})
export class AddStockComponent implements OnInit {

  @ViewChild('tabView') someInput: TabView;

  userform: FormGroup;
  files: any[] = [];
  Owners: dropDown[] = [];
  Units: dropDown[] = [];
  uploadURL: string = "http://localhost:56488/api/stock/uploadFiles";
  msgs: any[] = [];
  fileToken: string;
  title = 'internet-connection-check';
  status = 'ONLINE'; //initializing as online by default
  isConnected = true;
  index: number = 0;

  constructor(private fb: FormBuilder, private stck: StockService, private conn: ConnectionService) { }

  ngOnInit(): void {



    this.conn.monitor().subscribe(isConnected => {
      debugger;
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.status = "ONLINE";
      } else {
        this.status = "OFFLINE"
      }
      alert(this.status);
    });

    this.userform = this.fb.group({
      'StockName': new FormControl('', Validators.required),
      'StockCode': new FormControl('', Validators.required),
      'StockOwner': new FormControl('', Validators.required),
      'StockUnitType': new FormControl('', Validators.required),
      'StockUnit': new FormControl('', Validators.required),
      'description': new FormControl('')
    });

    this.fillUnits();
    this.fillOwners();

  }

  //populate function
  fillOwners() {
    this.stck.getEmployees().then((response) => {
      response.map((value) => {
        let ddl: dropDown = new dropDown()
        ddl.label = value.containerName
        ddl.value = value.containerId
        this.Owners.push(ddl);
      });

    }).catch()
  }

  fillUnits() {
    this.stck.getUnitList().then((response) => {
      response.map((value) => {
        let ddl: dropDown = new dropDown()
        ddl.label = value.unitName
        ddl.value = value.unitId
        this.Units.push(ddl);
      });

    }).catch()
  }

  //Submission function of the form
  onSubmit(formValue: any) {
    debugger;

    let stck: stock = new stock();

    stck.stockCode = formValue.StockCode
    stck.stockName = formValue.StockName
    stck.stockContainerId = formValue.StockOwner
    stck.quantity = formValue.StockUnit
    stck.unitType = formValue.StockUnitType
    stck.description = formValue.description
    stck.fileToken = this.fileToken

    this.saveStock(stck);

  }

  saveStock(formValue: any) {
    
    let response = this.stck.saveStockData(formValue)
      .then(response => {
        debugger;
        this.index = (this.index === 0) ? 2 : this.index - 1;
        this.userform.reset();

      });

  }

  uploader(data: any) {
    debugger;
    this.msgs = [];
    this.msgs.push({ severity: 'info', summary: 'Upload Message', detail: data.originalEvent.body.message });
    this.fileToken = data.originalEvent.body._Data;
  }

  onError(event) {

    debugger;

  }

  manualUpload() {

    debugger;

  }


}
