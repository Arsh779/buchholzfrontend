import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddStockComponent } from './add-stock.component';
import { FileUploadModule } from 'primeng/fileupload';
import { PanelModule } from 'primeng/panel';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { CalendarModule } from 'primeng/calendar';
import { MessageModule } from 'primeng/message';
import { ReactiveFormsModule } from '@angular/forms';
import { MessagesModule } from 'primeng/messages';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { StockService } from 'src/app/Services/stock.service';
import { global } from 'src/app/global';
import { TabViewModule } from 'primeng/tabview';

@NgModule({
  declarations: [AddStockComponent],
  imports: [
    CommonModule,
    FileUploadModule,
    PanelModule,
    InputTextModule,
    InputTextareaModule,
    FieldsetModule,
    ButtonModule,
    TableModule,
    MessagesModule,
    ReactiveFormsModule,
    MessageModule,
    FormsModule,
    CalendarModule,
    DropdownModule,
    TabViewModule
  ],
  providers: [
    StockService,
    global
  ]
  
 
})
export class AddStockModule { }
