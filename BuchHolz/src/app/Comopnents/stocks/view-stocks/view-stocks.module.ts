import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewStocksComponent } from './view-stocks.component';



@NgModule({
  declarations: [ViewStocksComponent],
  imports: [
    CommonModule
  ]
})
export class ViewStocksModule { }
