import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  items: any[] = []


  constructor() { }

  ngOnInit(): void {

    this.items = [
      {
        label: 'Customer',
        icon: 'pi pi-pw pi-file',
        items: [{
          label: 'Add Customer',
          routerLink: "./addCustomer"
        },
        {
          label: 'View Customer',
          routerLink: "./viewCustomers"
        }
        ]
      },
      {
        label: 'Edit',
        icon: 'pi pi-fw pi-pencil',
        items: [
          { label: 'Delete', icon: 'pi pi-fw pi-trash' },
          { label: 'Refresh', icon: 'pi pi-fw pi-refresh' }
        ]
      }
    ];
  }

}
