import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { CustomerService } from 'src/app/Services/customer.service';
import { MachineService } from 'src/app/Services/machine.service';
import { Dropdown } from 'primeng/dropdown';
import { dropDown } from 'src/app/Models/dropDown';
import { response } from 'src/app/Models/response';
import { isFormattedError } from '@angular/compiler';
import { customer, CustomerAddress, ContactPerson } from 'src/app/Models/customer';
import { Message } from 'primeng/api';


@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {
  userform: FormGroup;
  Addresses: FormArray;
  uploadURL: string = "http://localhost:56488/api/stock/uploadFiles";
  index: number = 0
  AddressType: dropDown[] = []
  Countries: dropDown[] = []
  Title: dropDown[] = []
  SelectedIdentity: number
  CustomerNumberDialog: boolean = false
  CustomerUniqueNumber: string
  msgs: Message[] = [];
  checked: boolean = true

  constructor(private fb: FormBuilder, private custServ: CustomerService, private machineServ: MachineService) { }

  ngOnInit(): void {

    this.userform = this.fb.group({
      'customerName': new FormControl('', Validators.required),
      'telephoneNumber': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.required),
      'addresses': this.fb.array([this.createAddress()]),
      'customerTitle': new FormControl('', Validators.required),
      'description': new FormControl(''),

    });

    this.Addresses = this.userform.get('addresses') as FormArray;
    this.getAddressTypes()
    this.getContries()
    this.getTitles()
  }

  createAddress(): FormGroup {
    let addresForm = this.fb.group({
      street: new FormControl('', Validators.required),
      houseNumber: new FormControl(null, Validators.required),
      city: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required),
      locationOnMap: new FormControl(false),
      mainAddress: new FormControl('', Validators.required),
      addressOfMachineLocations: new FormControl('', Validators.required),
      shippingAddress: new FormControl('', Validators.required),
      billingAddress: new FormControl('', Validators.required),
      postalCode: new FormControl('', Validators.required),
      lattitude: new FormControl('', [
        this.requiredIfValidator(() => addresForm.get("locationOnMap").value)
      ]),
      longitude: new FormControl('', [
        this.requiredIfValidator(() => addresForm.get("locationOnMap").value)
      ]),
      contactPerson: this.createContactPerson(),
    });


    return addresForm;
  }

  createContactPerson(): FormGroup {
    let contactPerson = this.fb.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      mobileNumber: new FormControl('', Validators.required)
    });

    return contactPerson;
  }

  requiredIfValidator(predicate) {
    return (formControl => {
      if (!formControl.parent) {
        return null;
      }
      if (predicate()) {
        return Validators.required(formControl);
      }
      return null;
    })
  }

  onSubmit(userform) {
    debugger;
    let cust = new customer()
    let Addresses: CustomerAddress[] = []

    let t = <customer>userform
    cust.customerName = userform.customerName
    cust.description = userform.description
    cust.email = userform.email
    cust.telephoneNumber = userform.telephoneNumber
    cust.customerTitle = parseInt(userform.customerTitle)
    
    userform.addresses.map(value => {
      let Address = new CustomerAddress()
      let Person = new ContactPerson()

      Address.country = parseInt(value.country)
      Address.houseNumber = parseInt(value.houseNumber)
      Address.locationOnMap = value.locationOnMap
      Address.longitude = value.longitude
      Address.street = value.street
      Address.postal = value.postalCode
      Address.city = value.city
      Address.addressOfMachineLocations = value.addressOfMachineLocations
      Address.mainAddress = value.mainAddress
      Address.shippingAddress = value.shippingAddress
      Address.billingAddress = value.billingAddress
      Address.lattitude = value.lattitude

      Person.email = value.contactPerson.email
      Person.firstName = value.contactPerson.firstName
      Person.lastName = value.contactPerson.lastName
      Person.mobileNumber = value.contactPerson.mobileNumber
      Person.telephone = value.contactPerson.telephone

      Address.contactPerson = Person
      Addresses.push(Address);
    });

    cust.addresses = Addresses



    this.custServ.saveCustomer(cust).then(response => {
      debugger;
      if(response.code === '200')
      {
        this.CustomerNumberDialog = true;
        this.CustomerUniqueNumber = response._Data.uniqueNumber
        this.msgs.push({severity:'info', summary:'Vom Kunden generiert', detail:'200'});
      }
    })
  }

  openNext() {
    this.index = (this.index === 2) ? 0 : this.index + 1;
  }

  openPrev() {
    this.index = (this.index === 0) ? 2 : this.index - 1;
  }

  getAddressTypes() {
    this.custServ.getAddressType()
      .then(response => {
        this.AddressType.length = 0
        response._Data.map(value => {
          let addressType = new dropDown()
          addressType.label = value.addressType
          addressType.value = value.addressTypeId.toString()
          addressType.disabled = false
          this.AddressType.push(addressType);
        })
      })

    this.AddressType
  }

  getContries() {
    this.custServ.getCountries()
      .then(response => {
        response._Data.map(value => {
          let addressType = new dropDown()
          addressType.label = value.countryName
          addressType.value = value.countryId.toString()
          this.Countries.push(addressType);
        })
      })

    this.AddressType
  }

  getTitles() {
    this.custServ.getTitle()
      .then(response => {
        response._Data.map(value => {
          let addressType = new dropDown()
          addressType.label = value.title
          addressType.value = value.titleId.toString()
          this.Title.push(addressType);
        })
      })

    this.AddressType
  }

  addMoreAddress(): void {
    this.Addresses = this.userform.get('addresses') as FormArray;
    this.Addresses.push(this.createAddress());
  }

  removeAddress(): void {
    this.Addresses = this.userform.get('addresses') as FormArray;
    var length = this.Addresses.length;
    this.Addresses.removeAt(length - 1);
  }

  addressTypeChangeEvent(event, identity) {

    if (this.SelectedIdentity == identity)
      this.AddressType.find(x => x.value == "1").disabled = false

    if (event.value === "1") {
      this.SelectedIdentity = identity
      this.AddressType.find(x => x.value == event.value).disabled = true
    }

  }

}
