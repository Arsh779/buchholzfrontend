import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'primeng/fileupload';
import { PanelModule } from 'primeng/panel';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { CalendarModule } from 'primeng/calendar';
import { MessageModule } from 'primeng/message';
import { ReactiveFormsModule } from '@angular/forms';
import { MessagesModule } from 'primeng/messages';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { global } from 'src/app/global';
import { TabViewModule } from 'primeng/tabview';
import { AddCustomerComponent } from './add-customer.component';
import { InputMaskModule } from 'primeng/inputmask';
import { CustomerService } from 'src/app/Services/customer.service';
import { CheckboxModule } from 'primeng/checkbox';
import { MachineService } from 'src/app/Services/machine.service';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  declarations: [AddCustomerComponent],
  imports: [
    CommonModule,
    BrowserModule,
    TabViewModule,
    PanelModule,
    InputTextModule,
    InputTextareaModule,
    FieldsetModule,
    CalendarModule,
    MessageModule,
    ReactiveFormsModule,
    MessagesModule,
    TableModule,
    FormsModule,
    ButtonModule,
    DropdownModule,
    InputMaskModule,
    CheckboxModule,
    DialogModule
  ],
  providers: [
    CustomerService,
    MachineService,
    global
  ]
})
export class AddCustomerModule { }
