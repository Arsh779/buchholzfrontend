import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/Services/customer.service';
import { customer } from 'src/app/Models/customer';

@Component({
  selector: 'app-view-customers',
  templateUrl: './view-customers.component.html',
  styleUrls: ['./view-customers.component.css']
})
export class ViewCustomersComponent implements OnInit {

  customers: customer[] = []

  constructor(private custServ: CustomerService) { }

  ngOnInit(): void {
    this.custServ.getAllCustomer().then(response => {
      response._Data.map(value => {
        debugger;
        let cust = new customer();

        cust.address = value.address
        cust.customerName = value.customerName
        cust.description = value.description
        cust.email = value.email
        cust.addresses = value.addresses

        this.customers.push(cust);
      })
    });
  }
  
}
