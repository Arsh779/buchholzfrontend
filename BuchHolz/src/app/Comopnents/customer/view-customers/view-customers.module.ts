import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewCustomersComponent } from './view-customers.component';
import { TableModule } from 'primeng/table'


@NgModule({
  declarations: [ViewCustomersComponent],
  imports: [
    CommonModule,
    TableModule
  ]
})
export class ViewCustomersModule { }
