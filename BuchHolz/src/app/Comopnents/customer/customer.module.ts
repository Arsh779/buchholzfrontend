import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerComponent } from './customer.component';
import { userLayoutModule } from '../layouts/user-layout/user-layout.module';
import { PanelModule } from 'primeng/panel';
import { Routes, RouterModule } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ListboxModule } from 'primeng/listbox';
import { StockService } from 'src/app/Services/stock.service';
import { global } from 'src/app/global';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ViewCustomersModule } from './view-customers/view-customers.module';
import { AddCustomerModule } from './add-customer/add-customer.module';


@NgModule({
  declarations: [CustomerComponent],
  imports: [
    CommonModule,
    userLayoutModule,
    PanelModule,
    ListboxModule,
    PanelMenuModule,
    RouterModule,
    ViewCustomersModule,
    AddCustomerModule
  ]
})
export class CustomerModule { }
