import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Materialverbrauch } from 'src/app/Models/Auftrag';
import { Table } from 'primeng/table/table';

@Component({
  selector: 'app-safety-test',
  templateUrl: './safety-test.component.html',
  styleUrls: ['./safety-test.component.css']
})
export class SafetyTestComponent implements OnInit {

  machine: string = "Machine XYZ";
  machineNumber: string = "DIN VDE 0701/0702";

  properties: any[];
  immissionControl: any[];
  hotwater: any[];
  functional: any[];
  clearClass: string = 'clear-signature';
  cleatText: string = 'löschen';


  userform: FormGroup;

  ngOnInit() {
    this.properties =
      [
        { testName: 'Leitungsystem Wasser', function: 'dicht' },
        { testName: 'Schwimmerventil', function: 'dicht, funktionstüchtig' },
        { testName: 'Schwingungsdämpfer', function: 'Vorspanndruck ausreichend' },
        { testName: 'Manometer', function: 'funktionstüchtig' },
        { testName: 'Sicherheitsventil', function: 'Öffnungsdruck' },
        { testName: 'Wassermangelsicherung', function: 'i.O.' },
        { testName: 'Druckschalter', function: 'Aus' },
        { testName: 'Reinigungsmittelsystem', function: 'dicht' },
        { testName: 'Beitriebsanweisung', function: 'vorhanden, lesbar' },
        { testName: 'Fabrikschild', function: 'lesbar, fest, angebracht' },
        { testName: 'Abdeckhaube ', function: 'vorhanden, Verriegelung i.O.' },
        { testName: 'Adbeckung umlaufender Teile vorhanden', function: '' },
        { testName: 'HD-Schläuche und Schlaucharmaturen', function: 'unbeschädigt, dicht, Kennzeichnung erkennbar' },
        { testName: 'Strahlrohr', function: 'dicht, unbeschädigt' },
        { testName: 'Netzanschluss', function: 'Kabel unbeschädigt' }
      ],

      this.immissionControl =
      [
        { testName: 'Abgasverlust in %', type: 'text' },
        { testName: 'CO2-Gehalt in %', type: 'text' },
        { testName: 'CO-Gehalt in ppm(nur Gas-Geräte)', type: 'text' },
        { testName: 'Rußbild(nach Ringelmann-Skala)', type: 'text' },
        { testName: 'Ölderivat im Abgas(Ja/Nein)', type: 'checkbox' }
      ],

      this.hotwater =
      [
        { testName: 'Durchlauferhitzer', function: 'entspricht den Abgaswerten' },
        { testName: 'Heizschlange', function: 'frei von Ablagerungen, Kalk etc. (Druck ohne Schlauch)' },
        { testName: 'Zündeinrichtung', function: 'Dauerzündung funktioniert, Elektrodenabstand i.O.' },
        { testName: 'Temperaturregler(-wächter)', function: 'schaltet Brenner/Gerät ab' },
        { testName: 'Brennstoffmagnetventil', function: 'funktionstüchtig' }
      ]

    this.functional =
      [
        {
          testName: 'Das Gerat wurd entsprechend der ' + this.machine
            + ' (Arbeiten mit Flussigkeitsstrahlern durch einen Sachkundigen) '
            + 'geprüft. Die dabei festgestellten Mängel wurden beseitigt, so dass die Arbeitssicherheit des Gerätes bestätigt wird',
          function: ''
        },
        {
          testName: 'Das Gerätes wurd entsprechend der ' + this.machine
            + ' (Arbeiten mit Flüssigkeitsstrahlern durch einen Sachkundigen) '
            + 'geprüft. Die Arbeitssicherheit der Gerätes ist erst nach Beseitigung der festgestellten Mangel gewährleistet',
          function: ''
        }
      ]
  }

}
