import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafetyTestComponent } from './safety-test.component';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { PanelModule } from 'primeng/panel';
import { InputTextModule } from 'primeng/inputtext';
import { SignaturePadModule } from '@ng-plus/signature-pad';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ReactiveFormsModule } from '@angular/forms'
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { userLayoutModule } from '../layouts/user-layout/user-layout.module';


@NgModule({
  declarations: [SafetyTestComponent],
  imports: [
    DropdownModule,
    FormsModule,
    CommonModule,
    CheckboxModule,
    PanelModule,
    InputTextModule,
    SignaturePadModule,
    InputTextareaModule,
    FieldsetModule,
    ButtonModule,
    TableModule,
    MessagesModule,
    ReactiveFormsModule,
    MessageModule,
    CalendarModule,
    TabViewModule,
    userLayoutModule
  ]
})
export class SafetyTestModule { }
