import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { StockService } from 'src/app/Services/stock.service';
import { dropDown } from 'src/app/Models/dropDown';
import { article } from 'src/app/Models/Article';
import { ArticleService } from 'src/app/Services/article.service';
import { stripSummaryForJitNameSuffix } from '@angular/compiler/src/aot/util';

@Component({
  selector: 'app-add-articles',
  templateUrl: './add-articles.component.html',
  styleUrls: ['./add-articles.component.css']
})
export class AddArticlesComponent implements OnInit {
  userform: FormGroup;
  Stocks: dropDown[] = [];
  Units: dropDown[] = [];
  files: any[] = [];
  uploadURL: string = "http://localhost:56488/api/Article/uploadFiles";
  token: any;
  index: number = 0;
  prefixes: dropDown[] = [];
  allSupliers: dropDown[] = [];

  constructor(private fb: FormBuilder, private stockServ: StockService, private artServ: ArticleService) {

  }

  ngOnInit(): void {
    this.userform = this.fb.group({
      'ArticleNumber': new FormControl('', Validators.required),
      'ArticleName': new FormControl('', Validators.required),
      'ArticleUnit': new FormControl('', Validators.required),
      'ArticleQuantity': new FormControl('', Validators.required),
      'Barcode': new FormControl('', Validators.required),
      'Price': new FormControl('', Validators.required),
      'SellingPrice': new FormControl('', Validators.required),
      'BuyingPrice': new FormControl('', Validators.required),
      'StockId': new FormControl('', Validators.required),
      'ArticleDescription': new FormControl('', Validators.required)
    });
    this.getallStocks()
    this.getallUnits()
    this.getSuppliers()
  }

  getallUnits() {
    this.stockServ.getUnitList().then((response) => {
      response.map((value) => {
        let ddl: dropDown = new dropDown()
        ddl.label = value.unitName
        ddl.value = value.unitId
        this.Units.push(ddl);
      });

    }).catch();
  }

  getallStocks() {
    this.stockServ.viewStocks().then(response => {
      response.map(value => {
        let ddl: dropDown = new dropDown()
        ddl.label = value.stockName
        ddl.value = value.stockId
        this.Stocks.push(ddl);
      })
    });
  }

  getSuppliers() {
    this.artServ.getSupplier().then(response => {
      response._Data.map(value => {
        let ddl: dropDown = new dropDown()
        ddl.label = value.supplierPrefix
        ddl.value = value.supplierName
        this.prefixes.push(ddl);
        this.allSupliers.push(ddl);
      })
    })
  }

  onSubmit(userform) {

    let art: article = new article();

    art.articleName = userform.ArticleName
    art.articleNumber = userform.ArticleNumber
    art.sellingPrice = userform.SellingPrice
    art.articleQuantity = userform.ArticleQuantity
    art.articleUnit = userform.ArticleUnit
    art.barcode = userform.Barcode
    art.stockId = userform.StockId
    art.buyingPrice = userform.BuyingPrice
    art.articleDescription = userform.ArticleDescription
    art.price = userform.Price
    art.fileToken = this.token

    this.artServ.saveArticles(art).then(response => {
      this.index = (this.index === 0) ? 2 : this.index - 1;
      this.userform.reset();
    });

  }

  uploader(event) {
    this.token = event.originalEvent.body._Data;
  }

  filterBrands(event) {
    this.prefixes = [];
    for (let i = 0; i < this.allSupliers.length; i++) {
      let supplier = this.allSupliers[i];
      if (supplier.label.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.prefixes.push(supplier);
      }
    }
  }

  selection(event){    
    this.userform.controls['ArticleNumber'].setValue(event.label);
  }
}
