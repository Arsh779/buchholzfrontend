import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddArticlesComponent } from './add-articles.component';
import { PanelModule } from 'primeng/panel';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { MessageModule } from 'primeng/message';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { TabViewModule } from 'primeng/tabview';
import { FileUploadModule } from 'primeng/fileupload';
import { AutoCompleteModule } from 'primeng/autocomplete';

@NgModule({
  declarations: [AddArticlesComponent],
  imports: [
    CommonModule,
    PanelModule,
    InputTextModule,
    InputTextareaModule,
    FieldsetModule,
    ButtonModule,
    TableModule,
    ReactiveFormsModule,
    MessageModule,
    FormsModule,
    CalendarModule,
    DropdownModule,
    TabViewModule,
    FileUploadModule,
    AutoCompleteModule
  ]
})
export class AddArticlesModule { }
