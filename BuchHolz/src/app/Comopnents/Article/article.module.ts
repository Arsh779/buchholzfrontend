import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article.component';
import { userLayoutModule } from '../layouts/user-layout/user-layout.module';
import { PanelModule } from 'primeng/panel';
import { Routes, RouterModule } from '@angular/router';
// import { FormBuilder } from '@angular/forms';
import { ListboxModule } from 'primeng/listbox';
import { global } from 'src/app/global';
import { PanelMenuModule } from 'primeng/panelmenu';
import { AddArticlesModule } from './add-articles/add-articles.module';
import {MessageModule} from 'primeng/message';
import {InputMaskModule} from 'primeng/inputmask';

@NgModule({
  declarations: [ArticleComponent],
  imports: [
    CommonModule,
    userLayoutModule,
    PanelModule,
    RouterModule,
    // FormBuilder,
    ListboxModule,
    PanelMenuModule,
    MessageModule,
    AddArticlesModule,
    InputMaskModule
  ]
})
export class ArticleModule { }
  