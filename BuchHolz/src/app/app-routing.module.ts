import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobsComponent } from './Comopnents/jobs/jobs.component';
import { SafetyTestComponent } from './Comopnents/safety-test/safety-test.component';
import { LoginComponent } from './Comopnents/login/login.component';
import { StocksComponent } from './Comopnents/stocks/stocks.component';
import { AddStockComponent } from './Comopnents/stocks/add-stock/add-stock.component';
import { ViewStocksComponent } from './Comopnents/stocks/view-stocks/view-stocks.component';
import { ArticleComponent } from './Comopnents/Article/article.component';
import { AddArticlesComponent } from './Comopnents/Article/add-articles/add-articles.component';
import { ViewArticleComponent } from './Comopnents/Article/view-article/view-article.component';
import { DefaultComponent } from './Comopnents/default/default.component';
import { CustomerComponent } from './Comopnents/customer/customer.component';
import { AddCustomerComponent } from './Comopnents/customer/add-customer/add-customer.component';
import { AddMachineComponent } from './Comopnents/machine/add-machine/add-machine.component';
import { ViewMachineComponent } from './Comopnents/machine/view-machine/view-machine.component';
import { ViewCustomersComponent } from './Comopnents/customer/view-customers/view-customers.component';
import { MachineComponent } from './Comopnents/machine/machine.component';
import { AttachCustomerComponent } from './Comopnents/machine/attach-customers/attach-customer.component';


const routes: Routes = [
  { path: 'jobsAssignment', component: JobsComponent },
  { path: 'safetyTest', component: SafetyTestComponent },
  { path: 'login', component: LoginComponent },
  { path: 'error', component: DefaultComponent },
  { path: 'error', component: DefaultComponent },
  {
    path: 'article', component: ArticleComponent,
    children: [
      { path: 'addArticle', component: AddArticlesComponent },
      { path: 'viewArticles', component: ViewArticleComponent }
    ]
  },
  {
    path: 'stocks', component: StocksComponent,
    children: [
      { path: 'addStocks', component: AddStockComponent },
      { path: 'viewArticles', component: ViewStocksComponent }
    ]
  },
  {
    path: 'customer', component: CustomerComponent,
    children: [
      { path: 'addCustomer', component: AddCustomerComponent },
      { path: 'addMachine', component: AddMachineComponent },
      { path: 'viewCustomers', component: ViewCustomersComponent }
    ]
  },
  {
    path: 'machine', component: MachineComponent,
    children: [
      { path: 'addMachine', component: AddMachineComponent },
      { path: 'viewMachines', component: ViewMachineComponent },
      { path: 'attachMachine', component: AttachCustomerComponent }
    
    ]
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/error', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
