import { HttpHeaders } from '@angular/common/http';

export class global {
    allowOriginSetting: string = "*";
    contentType: string = "application/json; charset=utf-8"

    getApiHeaders(): HttpHeaders {

        let headers = new HttpHeaders({
            'Access-Control-Allow-Origin': this.allowOriginSetting,
            'Content-Type': this.contentType,
            'Accept': 'application/json'
        });

        return headers;
    }

    baseURL: string = "http://localhost:56488";

    getOwners: string = this.baseURL + "/api/stock/getOwners"
    stockUnitList: string = this.baseURL + "/api/stock/getUnits";
    uploadFiles: string = this.baseURL + "/api/stock/uploadFiles";
    viewStock: string = this.baseURL + "/api/stock/uploadFiles";
    getStocks: string = this.baseURL + "/api/stock/getAllStocks";
    getAllMachines: string = this.baseURL + "/api/machine/getAllMachine";
    articleUnitList: string = this.baseURL + "/api/article/getUnits";
    getSuppliers: string = this.baseURL + "/api/article/getSuppliers";
    getMachineSpeciality: string = this.baseURL + "/api/machine/getMachineSpeciality";
    getMachineStatus: string = this.baseURL + "/api/machine/getMachineStatus";
    getMachineType: string = this.baseURL + "/api/machine/getMachineType";
    getMachineManufacturer: string = this.baseURL + "/api/machine/getMachineManufacturer";
    getMachineById: string = this.baseURL + "/api/machine/getMachineById/";
    getAllCustomers: string = this.baseURL + "/api/customer/getAllCustomers";
    getAddressTypes: string = this.baseURL + "/api/customer/getAddressTypes";
    getTitles: string = this.baseURL + "/api/customer/getTitles";
    getCustomerCountry: string = this.baseURL + "/api/customer/getCustomerCountry";
    getMachineTypeAttribute: string = this.baseURL + "/api/machine/getMachineTypeAttributes";

    saveStock: string = this.baseURL + "/api/stock/saveStock";
    saveArticle: string = this.baseURL + "/api/article/saveArticle";
    saveCustomer: string = this.baseURL + "/api/customer/saveCustomer";
    saveMachine: string = this.baseURL + "/api/machine/saveMachine";
    saveMachineForCustomer: string = this.baseURL + "/api/customer/saveMachineForCustomer";
}