import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JobSheetsModule } from './Comopnents/jobs/job-sheets.module';
import { SafetyTestModule } from './Comopnents/safety-test/safety-test.module';
import { LoginModule } from './Comopnents/login/login.module';
import { StocksModule } from './Comopnents/stocks/stocks.module';
import { HttpClientModule } from '@angular/common/http';
import { ArticleModule } from './Comopnents/Article/article.module';
import { DefaultComponent } from './Comopnents/default/default.component';
import { CustomerComponent } from './Comopnents/customer/customer.component';
import { CustomerModule } from './Comopnents/customer/customer.module';
import { DefaultModule } from './Comopnents/default/default.module';
import { MachineModule } from './Comopnents/machine/machine.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    JobSheetsModule,
    SafetyTestModule,
    StocksModule,
    LoginModule,
    NoopAnimationsModule,
    HttpClientModule,
    ArticleModule,
    CustomerModule,
    DefaultModule,
    MachineModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
