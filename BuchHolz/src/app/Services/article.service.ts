import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { global } from '../global';
import { response } from '../Models/response';
import { article } from '../Models/Article';
import { Supplier } from '../Models/Supplier';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http: HttpClient, private appValue: global) { }

  saveArticles(data: article){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.post<response<string>>(this.appValue.saveArticle, data, options).toPromise();
  }

  getSupplier(){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<Supplier[]>>(this.appValue.getSuppliers, options).toPromise();
  }

}
