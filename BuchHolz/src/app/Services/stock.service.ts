import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { global } from '../global';
import { stock } from '../Models/stock';
import { response } from '../Models/response';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private http: HttpClient, private appValue: global) { }

  getEmployees() {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<any>(this.appValue.getOwners, options).toPromise();

  }

  getUnitList() {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<any>(this.appValue.stockUnitList, options).toPromise();

  }

  saveStockData(data) {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.post<response<string>>(this.appValue.saveStock, data, options).toPromise();

  }

  uploadFiles(data) {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.post<any>(this.appValue.uploadFiles, data, options).toPromise();
  }

  viewStocks() {
    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<any>(this.appValue.getStocks, options).toPromise();
  }

}
