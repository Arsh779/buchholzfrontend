import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { global } from '../global';
import { response } from '../Models/response';
import { customer, customerMachine, CustomerAddressType, CustomerTitle, CustomerCountry } from '../Models/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  
  constructor(private http: HttpClient, private appValue: global) { }


  getAllCustomer(){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<customer[]>>(this.appValue.getAllCustomers, options).toPromise();
  }

  getAddressType(){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<CustomerAddressType[]>>(this.appValue.getAddressTypes, options).toPromise();
  }

  getTitle(){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<CustomerTitle[]>>(this.appValue.getTitles, options).toPromise();
  }

  
  getCountries(){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<CustomerCountry[]>>(this.appValue.getCustomerCountry, options).toPromise();
  }

  saveCustomer(data: customer){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.post<response<customer>>(this.appValue.saveCustomer, data, options).toPromise();
  }

  attachCustomer(data: customerMachine){

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.post<response<customerMachine>>(this.appValue.saveMachineForCustomer, data, options).toPromise();
  }




}
