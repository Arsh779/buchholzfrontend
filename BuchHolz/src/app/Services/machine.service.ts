import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { global } from '../global';
import { response } from '../Models/response';
import { Machine, MachineAttributeType } from '../Models/Machine';

@Injectable({
  providedIn: 'root'
})
export class MachineService {

  constructor(private http: HttpClient, private appValue: global) { }

  getMachineSpeciality() {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<any>(this.appValue.getMachineSpeciality, options).toPromise();

  }

  getMachineStatus() {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<any>(this.appValue.getMachineStatus, options).toPromise();

  }

  getMachineType() {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<any>(this.appValue.getMachineType, options).toPromise();

  }

  getMachineManufacturer() {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<any>(this.appValue.getMachineManufacturer, options).toPromise();

  }

  getAllMachines() {
    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<Machine[]>>(this.appValue.getAllMachines, options).toPromise();
  }

    getMachineById(MachineId: number) {
    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<Machine>>(this.appValue.getMachineById + MachineId, options).toPromise();
  }

  getMachineTypeAttribute()
  {
    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.get<response<MachineAttributeType[]>>(this.appValue.getMachineTypeAttribute, options).toPromise();
  }

  saveMachine(data: Machine) {

    let options = {
      headers: this.appValue.getApiHeaders(),
      withCredentials: true
    }
    return this.http.post(this.appValue.saveMachine, data, options).toPromise();

  }
}
