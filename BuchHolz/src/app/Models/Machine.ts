export class Machine {

    machineId: string
    machineSpecialtyId: string
    machineTypeId: string
    manufacturerId: string
    machineStatusId: string
    machineUqId: string
    machineNumber: string
    machineName: string
    address: string
    lattitude: string
    longitude: string
    testDate: string
    machinePhoto: string
    dateOfManufacture: string
    dateOfSelling: string
    barcode: string
    uvvValidDate: string
    isActive: boolean
    createdDate: string
    modifiedDate: string
    machineSpecialty: MachineSpecialty[]
    machineStatus: MachineStatus[]
    machineType: MachineType[]
    manufacturer: Manufacturer[]
}

export class MachineSpecialty {
    machineSpecialtyId: number
    specialty: string
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class MachineStatus {
    machineStatusId: number
    machineStatus: string
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class MachineType {
    machineTypeId: number
    machineType: string
    isActive: string
    createdDate: string
    modifiedDate: string
}

export class Manufacturer {
    ManufacturerId: number
    Manufacturer: string
    IsActive: boolean
    CreatedDate: string
    ModifiedDate: string
}

export class MachineAttributeType {
    machineTypeId: number
    machineType: string
    description: string
    isActive: string
    createdDate: string
    modifiedDate: string
}

export class MachineAttributes {
    machineryAttributeId: number
    machineryType: number
    customerDeviceDelivered: boolean
    deliveryDate: string
    garunteeEndDate: string
    usedMachine: boolean
    previousOwner: string
    isScrapped: boolean
    scrappedDate: string
    machineAccessories: MachineAccessoryAttribute[]
    machineSpareParts: MachineBuiltInSparePartsAttributes[]
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class MachineAccessoryAttribute {
    accessoriesId: number
    accessory: string
    description: string
    machineryAttributeId: number
    isActive: number
    createdDate: string
    modifiedDate: string
}

export class MachineBuiltInSparePartsAttributes {
    builtInSparePartId: number
    sparePartName: string
    description: string
    machineryAttributeId: number
    isActive: boolean
    createdDate: string
    modifiedDate: string
}