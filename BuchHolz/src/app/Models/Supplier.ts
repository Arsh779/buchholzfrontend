export class Supplier {
    supplierId: string
    supplierName: string
    supplierPrefix: string
    supplierAddress: string
    supplierPhone: string
    supplierEmail: string
    isActive: string
    createdDate: string
    mdifiedDate: string
}