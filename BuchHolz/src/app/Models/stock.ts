export class stock {
    stockName: string
    stockCode: string
    unitType: number
    quantity: string
    stockContainerId: number
    description: string
    fileToken: string
}