export class article {
    articleNumber: string
    articleName: string
    articleUnit: string
    articleQuantity: string
    barcode: string
    price: string
    sellingPrice: string
    buyingPrice: string
    stockId: string
    articleDescription: string
    fileToken: string
}