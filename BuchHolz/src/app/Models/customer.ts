export class customer {
    customerId: number
    customerTitle: number
    customerName: string
    telephoneNumber: string
    email: string
    description: string
    address: number
    uniqueNumber: string
    addresses: CustomerAddress[]
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class customerMachine {
    custMachineId: number
    customerId: number
    machineId: number
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class CustomerAddress {
    customerAddressId: number
    mainAddress: boolean
    shippingAddress: boolean
    addressOfMachineLocations: boolean
    billingAddress: boolean
    customerId: number
    street: string
    locationOnMap: boolean
    lattitude: string
    longitude: string
    houseNumber: number
    postal: string
    city: string
    country: number
    addressType: number
    contactPersonId: number
    isActive: boolean
    createdDate: string
    modifiedDate: string
    contactPerson: ContactPerson
}

export class CustomerTitle {
    titleId: number
    title: string
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class ContactPerson {
    contactPersonId: number
    firstName: string
    lastName: string
    telephone: string
    email: string
    mobileNumber: string
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class CustomerAddressType {
    addressTypeId: number
    addressType: string
    isActive: boolean
    createdDate: string
    modifiedDate: string
}

export class CustomerCountry {
    countryId: number
    countryName: string
    isActive: string
    createdDate: string
    modifiedDate: string
}

