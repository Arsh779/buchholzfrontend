export class Sicherheitsprüfung
{
    operator: string
    plantNo: string
    manufactureYear: string
    deviceType: string
    customerNo: string
}